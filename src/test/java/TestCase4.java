import org.junit.Assert;
import org.junit.Test;

import edu.princeton.cs.algs4.In;

public class TestCase4 {

    @Test
    public void test_dictionary_yawl() {
        int[] points = { 0, 1, 100, 1000, 1111, 1250, 13464, 1500, 2, 200, 2000, 26539, 3, 300, 4, 400, 4410, 4527,
                4540, 5, 500, 750, 777 };

        In in = new In("D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\dictionary-yawl.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);

        for (int p : points) {
            String file = "D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\board-points" + p + ".txt";
            BoggleBoard board = new BoggleBoard(file);
            int score = 0;
            for (String word : solver.getAllValidWords(board)) {
                score += solver.scoreOf(word);
            }
            Assert.assertEquals(file, p, score);
        }
    }
    

    @Test
    public void test_unbalanced_board() {        
        In in = new In("D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\dictionary-yawl.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);

        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\board-antidisestablishmentarianisms.txt";
        BoggleBoard board = new BoggleBoard(file);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            score += solver.scoreOf(word);
        }
        Assert.assertEquals(file, 172, score);        
        
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\board-dichlorodiphenyltrichloroethanes.txt";
        board = new BoggleBoard(file);
        score = 0;
        for (String word : solver.getAllValidWords(board)) {
            score += solver.scoreOf(word);
        }
        Assert.assertEquals(file, 68, score);
        
    }

    @Test
    public void test_9q_board() {     
        In in = new In("D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\dictionary-16q.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver1 = new BoggleSolver(dictionary);
        BoggleSolver2 solver2 = new BoggleSolver2(dictionary);

        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\board-16q.txt";
        BoggleBoard board = new BoggleBoard(file);
        int score1 = 0;
        for (String word : solver1.getAllValidWords(board)) {
            score1 += solver1.scoreOf(word);
        }
        int score2 = 0;
        for (String word : solver2.getAllValidWords(board)) {
            score2 += solver2.scoreOf(word);
        }
        Assert.assertEquals(file, score1, score2);  
    }

    @Test
    public void test_2letters() {   
        In in = new In("D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\dictionary-2letters.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);

        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\boggle\\board-points4410.txt";
        BoggleBoard board = new BoggleBoard(file);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            score += solver.scoreOf(word);
        }
        Assert.assertEquals(file, 0, score);    
    }
}
