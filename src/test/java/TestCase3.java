import org.junit.Assert;
import org.junit.Test;

public class TestCase3 {

    private boolean contains(Iterable<String> it, String[] R) {
        int i = 0;
        for (String s : it) {
            if (i >= R.length || !R[i++].equals(s))
                return false;
        }
        return true;
    }

    @Test
    public void test_abc() {
        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\baseball\\teams4.txt";
        BaseballElimination division = new BaseballElimination(file);
        Assert.assertTrue("Atlanta is not eliminated", !division.isEliminated("Atlanta"));
        Assert.assertTrue("Philadelphia is eliminated", division.isEliminated("Philadelphia"));
        Assert.assertTrue("Philadelphia is eliminated", contains(division.certificateOfElimination("Philadelphia"), new String[] { "Atlanta", "New_York" }));
        Assert.assertTrue("New_York is not eliminated", !division.isEliminated("New_York"));
        Assert.assertTrue("Montreal is eliminated", division.isEliminated("Montreal"));
        Assert.assertTrue("Philadelphia is eliminated", contains(division.certificateOfElimination("Montreal"), new String[] { "Atlanta" }));

        file = "D:\\projects\\RnD\\princeton\\algo_partII\\baseball\\teams5.txt";
        division = new BaseballElimination(file);
        Assert.assertTrue("New_York is not eliminated", !division.isEliminated("New_York"));
        Assert.assertTrue("Baltimore is eliminated", !division.isEliminated("Baltimore"));
        Assert.assertTrue("Boston is not eliminated", !division.isEliminated("Boston"));
        Assert.assertTrue("Toronto is not eliminated", !division.isEliminated("Toronto"));
        Assert.assertTrue("Detroit is eliminated", division.isEliminated("Detroit"));
        Assert.assertTrue("Philadelphia is eliminated", contains(division.certificateOfElimination("Detroit"), new String[] { "New_York", "Baltimore", "Boston", "Toronto" }));
    }
}
