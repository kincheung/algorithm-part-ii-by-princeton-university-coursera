import org.junit.Assert;
import org.junit.Test;

import edu.princeton.cs.algs4.Picture;

public class TestCase2 {

    private void shouldRemoveVerticalSeamThrowIllegalArgumentException(SeamCarver carver, int[] seam) {
        try {
            carver.removeVerticalSeam(seam);
            Assert.fail();
        } catch (IllegalArgumentException e) {}
    }

    @Test
    public void test_removeVerticalSeam() {
        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\10x10.png";
        Picture picture = new Picture(file);
        SeamCarver carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ 1, 1, 0, 1, 2, 1, 1, 0, -1, 0 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\3x7.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ -1, -1, 0, 0, 1, 2, 2 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\7x3.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ 0, 1 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\10x12.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ 1, 0, 0, 1, 2, 3, 2, 1, 1, 2, 3, 1 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\12x10.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ -1, 0, 0, 0, 1, 1, 2, 1, 0, 0 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\1x8.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveVerticalSeamThrowIllegalArgumentException(carver, new int[]{ -1, 0, 0, 0, 0, -1, 0, 0 });
    }
    
    

    private void shouldRemoveHorizontalSeamThrowIllegalArgumentException(SeamCarver carver, int[] seam) {
        try {
            carver.removeVerticalSeam(seam);
            Assert.fail();
        } catch (IllegalArgumentException e) {}
    }

    @Test
    public void test_removeHorizontalSeam() {
        String file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\10x10.png";
        Picture picture = new Picture(file);
        SeamCarver carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ -1, 0, 1, 1, 0, 1, 0, 1, 0, 0 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\3x7.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ 0, 0, -1 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\7x3.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ -1, 0, 0, 1, 2, 2, 1 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\10x12.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ 8, 7, 6, 7, 5, 6, 8, 7, 8, 7 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\12x10.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ 8, 9, 9, 8, 7, 7, 8, 7, 8, 6, 5, 3 });
        file = "D:\\projects\\RnD\\princeton\\algo_partII\\src\\main\\resources\\1x8.png";
        picture = new Picture(file);
        carver = new SeamCarver(picture);        
        shouldRemoveHorizontalSeamThrowIllegalArgumentException(carver, new int[]{ -1, 0, 0, -1, 0, 0, 0, 0 });
    }
}
