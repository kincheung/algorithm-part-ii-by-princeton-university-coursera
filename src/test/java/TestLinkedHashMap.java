import java.util.LinkedHashMap;

public class TestLinkedHashMap {

    public static void main(String[] args) {
        LinkedHashMap<Integer, Integer> m = new LinkedHashMap<>();
        m.put(11,1);
        m.put(22,2);
        m.put(33,3);
        m.put(44,4);
        
        for (Integer i: m.keySet()) {
            System.err.println(i);
        }
    }

}
