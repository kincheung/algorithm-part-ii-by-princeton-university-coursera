import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class TestCase1 {

    private void newWordNet_throwException(String s1, String s2) {
        try {
            new WordNet(s1, s2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void test_wordnet_invalidGraph() {
        newWordNet_throwException("wordnet/synsets3.txt", "wordnet/hypernyms3InvalidCycle.txt");
        newWordNet_throwException("wordnet/synsets3.txt", "wordnet/hypernyms3InvalidTwoRoots.txt");

        newWordNet_throwException("wordnet/synsets6.txt", "wordnet/hypernyms6InvalidCycle.txt");
        newWordNet_throwException("wordnet/synsets6.txt", "wordnet/hypernyms6InvalidCycle+Path.txt");
        newWordNet_throwException("wordnet/synsets6.txt", "wordnet/hypernyms6InvalidTwoRoots.txt");
    }

    @Test
    public void test_wordnet_big() {
        new WordNet("wordnet/synsets100-subgraph.txt", "wordnet/hypernyms100-subgraph.txt");

        new WordNet("wordnet/synsets100-subgraph.txt", "wordnet/hypernyms100-subgraph.txt");
        new WordNet("wordnet/synsets1000-subgraph.txt", "wordnet/hypernyms1000-subgraph.txt");
        new WordNet("wordnet/synsets10000-subgraph.txt", "wordnet/hypernyms10000-subgraph.txt");

        new WordNet("wordnet/synsets500-subgraph.txt", "wordnet/hypernyms500-subgraph.txt");
        new WordNet("wordnet/synsets5000-subgraph.txt", "wordnet/hypernyms5000-subgraph.txt");
        new WordNet("wordnet/synsets50000-subgraph.txt", "wordnet/hypernyms50000-subgraph.txt");
    }

    @Test
    public void test_wordnet_Tree() {
        WordNet wordNet = new WordNet("wordnet/synsets15.txt", "wordnet/hypernyms15Tree.txt");

        // h=2, o=5
        String nounA = "h", nounB = "o";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 7);
        Assert.assertEquals(sap, "b");

        nounA = "o";
        nounB = "h";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 7);
        Assert.assertEquals(sap, "b");

        // c=1, o=6
        nounA = "c";
        nounB = "o";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 7);
        Assert.assertEquals(sap, "a");

        nounA = "o";
        nounB = "c";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 7);
        Assert.assertEquals(sap, "a");

        // g=1, j=2
        nounA = "g";
        nounB = "j";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 3);
        Assert.assertEquals(sap, "f");

        nounA = "j";
        nounB = "g";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 3);
        Assert.assertEquals(sap, "f");

        // g=1, j=2
        nounA = "d";
        nounB = "f";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "b");

        nounA = "f";
        nounB = "d";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "b");

        // o=0, o=0
        nounA = "o";
        nounB = "o";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 0);
        Assert.assertEquals(sap, "o");
    }

    @Test
    public void test_wordnet_Path() {
        WordNet wordNet = new WordNet("wordnet/synsets15.txt", "wordnet/hypernyms15Path.txt");

        // a=0, o=14
        String nounA = "a", nounB = "o";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 14);
        Assert.assertEquals(sap, "o");

        nounA = "o";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 14);
        Assert.assertEquals(sap, "o");
    }

    @Test
    public void test_wordnet_ManyPathsOneAncestor() {
        WordNet wordNet = new WordNet("wordnet/synsets11.txt", "wordnet/hypernyms11ManyPathsOneAncestor.txt");

        // a=2, g=2
        String nounA = "a", nounB = "k";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "f");

        nounA = "k";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "f");

        // f=0, f=0
        nounA = "f";
        nounB = "f";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 0);
        Assert.assertEquals(sap, "f");

        // b=1, j=1
        nounA = "b";
        nounB = "j";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "f");

        nounA = "j";
        nounB = "b";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "f");
    }

    @Test
    public void test_wordnet_AmbiguousAncestor() {
        WordNet wordNet = new WordNet("wordnet/synsets11.txt", "wordnet/hypernyms11AmbiguousAncestor.txt");

        // a=2, g=2
        String nounA = "a", nounB = "g";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "c");

        nounA = "g";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "c");

        // a=2, d=2
        nounA = "a";
        nounB = "d";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "c");

        nounA = "d";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "c");

        // a=2, g=2
        nounA = "g";
        nounB = "k";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "h");

        nounA = "k";
        nounB = "g";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "h");

        // a=2, g=2
        nounA = "g";
        nounB = "d";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 3);
        Assert.assertEquals(sap, "e");

        nounA = "d";
        nounB = "g";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 3);
        Assert.assertEquals(sap, "e");
    }

    @Test
    public void test_wordnet_wrongBFS() {
        WordNet wordNet = new WordNet("wordnet/synsets8.txt", "wordnet/hypernyms8WrongBFS.txt");

        // e=4, a=1
        String nounA = "e", nounB = "a";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 5);
        Assert.assertEquals(sap, "h");

        nounA = "a";
        nounB = "e";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 5);
        Assert.assertEquals(sap, "h");

        // b=0, a=1
        nounA = "b";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "b");

        nounA = "a";
        nounB = "b";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "b");

        // c=0, a=2
        nounA = "c";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "c");

        nounA = "a";
        nounB = "c";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertEquals(sap, "c");
    }

    @Test
    public void test_wordnet_manyAncestors() {
        WordNet wordNet = new WordNet("wordnet/synsets8.txt", "wordnet/hypernyms8ManyAncestors.txt");

        // a=1, e=0
        String nounA = "a", nounB = "e";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "e");

        nounA = "e";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "e");

        // a=1, h=0
        nounA = "a";
        nounB = "h";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "h");

        nounA = "h";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "h");

        // a=1, c=1
        nounA = "a";
        nounB = "c";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 2);
        Assert.assertTrue("bdefgh".contains(sap));
    }

    @Test
    public void test_wordnet_selfAncestors() {
        WordNet wordNet = new WordNet("wordnet/synsets6.txt", "wordnet/hypernyms6TwoAncestors.txt");

        // a=0, c=0
        String nounA = "a", nounB = "a";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 0);
        Assert.assertEquals(sap, "a");

        // a=0, c=4
        nounA = "a";
        nounB = "c";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "a");

        nounA = "c";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 4);
        Assert.assertEquals(sap, "a");

        // b=1, a=0
        nounA = "b";
        nounB = "a";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "a");

        nounA = "a";
        nounB = "b";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 1);
        Assert.assertEquals(sap, "a");
    }

    @Test
    public void test_wordnet() {

        WordNet wordNet = new WordNet("wordnet/synsets.txt", "wordnet/hypernyms.txt");

//        while(true) {
        String nounA = "worm", nounB = "bird";
        int distance = wordNet.distance(nounA, nounB);
        String sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 5);
        Assert.assertEquals(sap, "animal animate_being beast brute creature fauna");

        nounA = "municipality";
        nounB = "region";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 3);
        Assert.assertEquals(sap, "region");

        nounA = "individual";
        nounB = "edible_fruit";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 7);
        Assert.assertEquals(sap, "physical_entity");

        nounA = "white_marlin";
        nounB = "mileage";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 23);

        nounA = "Black_Plague";
        nounB = "black_marlin";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 33);

        nounA = "American_water_spaniel";
        nounB = "histology";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 27);

        nounA = "Brown_Swiss";
        nounB = "barrel_roll";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        Assert.assertEquals(distance, 29);

        nounA = "bed";
        nounB = "coffee";
        distance = wordNet.distance(nounA, nounB);
        sap = wordNet.sap(nounA, nounB);
//        System.out.println(String.format("%s to %s: distance: %d, sap: %s", nounA, nounB, distance, sap));
        
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        }
    }

    private void test_all_sap(String outcastFile) {
        In in = new In(outcastFile);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        SAP_ACE sapAce = new SAP_ACE(G);
        
        for(int v=0; v<G.V(); ++v){
            for(int w=0; w<G.V(); ++w){
                Assert.assertEquals(String.format("v = %d, w = %d", v, w), sapAce.length(v, w), sap.length(v, w));
//                Assert.assertEquals(String.format("v = %d, w = %d", v, w), sapAce.ancestor(v, w), sap.ancestor(v, w));
            }
        }
    }
    
    private void test_sap_random(String outcastFile) {
        In in = new In(outcastFile);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        SAP_ACE sapAce = new SAP_ACE(G);
        
        for(int i=0; i<100_000; ++i){
            int v = (int)Math.random()*G.V();
            int w = (int)Math.random()*G.V();
            Assert.assertEquals(String.format("v = %d, w = %d", v, w), sapAce.length(v, w), sap.length(v, w));
            Assert.assertEquals(String.format("v = %d, w = %d", v, w), sapAce.ancestor(v, w), sap.ancestor(v, w));
            
        }
    }

    @Test
    public void test_sap_indexOutOfBounds() {

        String outcastFile = "wordnet/digraph1.txt";
        In in = new In(outcastFile);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        int v = -1;
        int w = 0;
        try {
            sap.length(v, w);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {
        }
        try {
            sap.length(v, w);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {
        }

        try {
            sap.length(Arrays.asList(new Integer[]{-1, 2}), Arrays.asList(new Integer[]{3, -4}));
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {
        }
        try {
            sap.length(Arrays.asList(new Integer[]{-1, 2}), Arrays.asList(new Integer[]{3, -4}));
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {
        }
    }
    
    @Test
    public void test_sap() {
        test_all_sap("wordnet/digraph1.txt");
        test_all_sap("wordnet/digraph2.txt");
        test_all_sap("wordnet/digraph3.txt");
        test_all_sap("wordnet/digraph4.txt");
        test_all_sap("wordnet/digraph5.txt");
        test_all_sap("wordnet/digraph6.txt");
        test_all_sap("wordnet/digraph9.txt");
        test_sap_random("wordnet/digraph-wordnet.txt");
        
//      String outcastFile = "wordnet/digraph2.txt";
//      In in = new In(outcastFile);
//      Digraph G = new Digraph(in);
//      SAP sap = new SAP(G);
//      int v = 1;
//      int w = 3;
//      int length = sap.length(v, w);
//      int ancestor = sap.ancestor(v, w);
//      StdOut.printf(outcastFile + " length = %d, ancestor = %d\n", length, ancestor);
//      Assert.assertEquals(2, length);
//      Assert.assertEquals(3, ancestor);

//      outcastFile = "wordnet/digraph-wordnet.txt";
//      in = new In(outcastFile);
//      G = new Digraph(in);
//      sap = new SAP(G);
//      v = 70860;
//      w = 18158;
//      length = sap.length(v, w);
//      ancestor = sap.ancestor(v, w);
//      StdOut.printf(outcastFile + " length = %d, ancestor = %d\n", length, ancestor);
//      Assert.assertEquals(14, length);
//      Assert.assertEquals(38003, ancestor);
//
//        v = 32348;
//        w = 344;
//        length = sap.length(v, w);
//        ancestor = sap.ancestor(v, w);
//        StdOut.printf(outcastFile + " length = %d, ancestor = %d\n", length, ancestor);
//        Assert.assertEquals(15, length);
//        Assert.assertEquals(18840, ancestor);
    }

    @Test
    public void test_outcast() {
        WordNet wordNet = new WordNet("wordnet/synsets.txt", "wordnet/hypernyms.txt");
        Outcast outcast = new Outcast(wordNet);

        In in = null;
        String outcastFile = "wordnet/outcast3.txt";
        in = new In(outcastFile);
        String[] nouns = in.readAllStrings();
        String result = outcast.outcast(nouns);
        StdOut.println(outcastFile + ": " + result);
        Assert.assertEquals("Mickey_Mouse", result);

        outcastFile = "wordnet/outcast4.txt";
        in = new In(outcastFile);
        nouns = in.readAllStrings();
        result = outcast.outcast(nouns);
        StdOut.println(outcastFile + ": " + result);
        Assert.assertEquals("probability", result);

        outcastFile = "wordnet/outcast5.txt";
        in = new In(outcastFile);
        nouns = in.readAllStrings();
        result = outcast.outcast(nouns);
        StdOut.println(outcastFile + ": " + result);
        Assert.assertEquals("table", result);

        System.out.println("wordnet/outcast8.txt");
        outcastFile = "wordnet/outcast8.txt";
        in = new In(outcastFile);
        nouns = in.readAllStrings();
        result = outcast.outcast(nouns);
        StdOut.println(outcastFile + ": " + result);
        Assert.assertEquals("bed", result);

        outcastFile = "wordnet/outcast11.txt";
        in = new In(outcastFile);
        nouns = in.readAllStrings();
        result = outcast.outcast(nouns);
        StdOut.println(outcastFile + ": " + result);
        Assert.assertEquals("potato", result);
    }

    @Test
    public void test_IllegalArgumentException() {
        WordNet wordNet = new WordNet("wordnet/synsets.txt", "wordnet/hypernyms.txt");

        String nounA = "worm", nounB = "bird";
        try {
            wordNet.distance("xxx", nounB);
            Assert.fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        }
        try {
            wordNet.distance(nounA, "yyy");
            Assert.fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        }

        try {
            wordNet.sap("xxx", nounB);
            Assert.fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        }

        try {
            wordNet.sap(nounA, "yyy");
            Assert.fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        }

    }

    @Test
    public void test_NullPointerException() {
        WordNet wordNet = new WordNet("wordnet/synsets.txt", "wordnet/hypernyms.txt");

        String nounA = "worm", nounB = "bird";
        try {
            wordNet.distance(null, nounB);
            Assert.fail("expected NullPointerException");
        } catch (NullPointerException e) {
        }
        try {
            wordNet.distance(nounA, null);
            Assert.fail("expected NullPointerException");
        } catch (NullPointerException e) {
        }

        try {
            wordNet.sap(null, nounB);
            Assert.fail("expected NullPointerException");
        } catch (NullPointerException e) {
        }

        try {
            wordNet.sap(nounA, null);
            Assert.fail("expected NullPointerException");
        } catch (NullPointerException e) {
        }

        try {
            wordNet.isNoun(null);
            Assert.fail("expected NullPointerException");
        } catch (NullPointerException e) {
        }

    }
}
