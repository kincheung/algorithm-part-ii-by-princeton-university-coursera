import java.awt.Color;

import edu.princeton.cs.algs4.Picture;

public class SeamCarver {
    private int[][] picturePixels;
    private int width;
    private int height;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null)
            throw new NullPointerException();

        
        int[][] temp = new int[picture.height()][picture.width()];
        for (int row = 0; row < picture.height(); ++row) {
            for (int col = 0; col < picture.width(); ++col) {
                Color color = picture.get(col, row);
                temp[row][col] =  color.getRGB();
            }
        }

        setPicturePixels(temp);
    }

    // current picture
    public Picture picture() {
        if (picturePixels != null) {

//            if (transposed)
//                transposeBack();
            
            int newHeight = height(), newWidth = width();
            Picture newPicture = new Picture(newWidth, newHeight);

            // StdOut.println("picture() newHeight: h:" + newHeight+", w:"+newWidth);
            for (int row = 0; row < newHeight; ++row) {
                for (int col = 0; col < newWidth; ++col) {
                    Color color = new Color(picturePixels[row][col]);
                    newPicture.set(col, row, color);
                }
            }
            // picturePixels = null;
            return new Picture(newPicture);
        }

        return null;
    }

    private void setPicturePixels(int[][] picturePixels) {
        this.picturePixels = picturePixels;
        width = picturePixels[0].length;
        height = picturePixels.length;
    }

    // width of current picture
    public int width() {
        return width;
    }

    // height of current picture
    public int height() {
        return height;
    }

    // energy of pixel at column x and row y
    public double energy(int x, int y) {

//        if (transposed && !processing)
//            transposeBack();
        
        if (x < 0 || x > width() - 1 || y < 0 || y > height() - 1) {
            throw new IndexOutOfBoundsException("x: " + x + " y: " + y);
        }

        if (x == 0 || y == 0 || x == width() - 1 || y == height() - 1)
            return 1000;
        Color left, right, top, bottom;

        left = new Color(picturePixels[y][x - 1]);
        right = new Color(picturePixels[y][x + 1]);
        bottom = new Color(picturePixels[y - 1][x]);
        top = new Color(picturePixels[y + 1][x]);

        double xr, xg, xb, yr, yg, yb;
        xr = right.getRed() - left.getRed();
        xg = right.getGreen() - left.getGreen();
        xb = right.getBlue() - left.getBlue();
        yr = top.getRed() - bottom.getRed();
        yg = top.getGreen() - bottom.getGreen();
        yb = top.getBlue() - bottom.getBlue();

        return Math.sqrt((xr * xr) + (xg * xg) + (xb * xb) + (yr * yr) + (yg * yg) + (yb * yb));
    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        // StdOut.println("findHorizontalSeam before: h:" + height() + " w:" +width());
//        if (!this.transposed) {
             int[][] old = picturePixels;
            transpose();
//        }
        // StdOut.println("findHorizontalSeam after: h:" + height() + " w:" +width());
//        fromHorizontal = true;
        int[] seam = findVerticalSeam();
         setPicturePixels(old);
//        fromHorizontal = false;
        return seam;
    }

    private void transpose() {
//        StdOut.println("transpose: before h:" + height() + " w:" + width());
        int[][] trans = new int[width()][height()]; // swap height and width
        for (int row = 0; row < height(); ++row) {
            for (int col = 0; col < width(); ++col) {
                trans[col][row] = picturePixels[row][col]; 
            }
        }
        setPicturePixels(trans);
//        StdOut.println("transpose: after h:" + height() + " w:" + width());
//        this.transposed = true;
    }

    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
//        processing = true;

//        if (transposed && !fromHorizontal) {
//            transposeBack();
//        }

//         StdOut.println("findVerticalSeam: h:" + height() + " w:" + width());
        int[][] parents = new int[width()][height()];
        double[] dpUpper = new double[width()];
        double[] dpLower = new double[width()];

        for (int col = 0; col < width(); ++col) {
            dpUpper[col] = 1000;
        }

        // StdOut.println();
        // for (int col = 0; col < width(); ++col)
        // StdOut.printf("%7.2f ", dpUpper[col]);
        // StdOut.println();

        double energy = Double.POSITIVE_INFINITY;
        double energyLeft = 0, energyMid = 0, energyRight = 0;
        for (int row = 0; row < height() - 1; ++row) {
            energyMid = energy(0, row + 1);
            dpLower[0] = Double.POSITIVE_INFINITY;
            for (int col = 0; col < width(); ++col) {
                if (col < width() - 1) {
                    energyRight = energy(col + 1, row + 1);
                    dpLower[col + 1] = Double.POSITIVE_INFINITY;
                }

                // bottom-left
                if (col > 0) {
                    energy = energyLeft;
                    if (dpLower[col - 1] > energy + dpUpper[col]) {
                        dpLower[col - 1] = energy + dpUpper[col];
                        parents[col - 1][row + 1] = col;
                    }
                }

                // bottom
                energy = energyMid;
                if (dpLower[col] > energy + dpUpper[col]) {
                    dpLower[col] = energy + dpUpper[col];
                    parents[col][row + 1] = col;
                }

                // bottom-right
                if (col < width() - 1) {
                    energy = energyRight;
                    if (dpLower[col + 1] > energy + dpUpper[col]) {
                        dpLower[col + 1] = energy + dpUpper[col];
                        parents[col + 1][row + 1] = col;
                    }
                }

                energyLeft = energyMid;
                energyMid = energyRight;
            }

            // for (int col = 0; col < width(); ++col)
            // StdOut.printf("%7.2f ", dpLower[col]);
            // StdOut.println();

            System.arraycopy(dpLower, 0, dpUpper, 0, dpLower.length); // dpUpper = dpLower;
        }

        // StdOut.println();
        // for (int row = 0; row < height(); ++row) {
        // for (int col = 0; col < width(); ++col) {
        // StdOut.printf("%d ", parents[col][row]);
        // }
        // StdOut.println();
        // }

        double end = Double.POSITIVE_INFINITY;
        int endpoint = 0;
        for (int col = 0; col < width(); ++col) {
            if (end > dpLower[col]) {
                end = dpLower[col];
                endpoint = col;
            }
        }

        // StdOut.println("endpoint: " + endpoint);

        int[] sp = new int[height()];
        sp[height() - 1] = endpoint;
        for (int i = height() - 2; i >= 0; --i) {
            sp[i] = parents[sp[i + 1]][i + 1];
        }

//        processing = false;
        
        return sp;
    }

//    private boolean transposed = false;
    private boolean fromHorizontal = false;
//    private boolean processing = false;

    // remove horizontal seam from current picture
    // 1. Throw a java.lang.IllegalArgumentException if removeVerticalSeam() or removeHorizontalSeam() is called with
    // an array of the wrong length or if the array is not a valid seam (i.e., either an entry is outside its prescribed range or two
    // adjacent entries differ by
    // more than 1).
    public void removeHorizontalSeam(int[] seam) {
        if (seam == null)
            throw new NullPointerException();
//        if (!transposed)
            transpose();
        fromHorizontal = true;
        removeVerticalSeam(seam);
        fromHorizontal = false;
        transposeBack();
    }

    private void transposeBack() {
//        StdOut.println("transposeBack: before h:" + height() + " w:" + width());
        int newHeight = width(), newWidth = height();
        int[][] seamedPixels = new int[newHeight][newWidth];

        for (int row = 0; row < newHeight; ++row) {
            for (int col = 0; col < newWidth; ++col) {
                seamedPixels[row][col] = picturePixels[col][row];
            }
        }
        setPicturePixels(seamedPixels);
//        StdOut.println("transposeBack: after h:" + height() + " w:" + width());
//        transposed = false;
    }

    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        if (seam == null)
            throw new NullPointerException();

//        if (transposed && !fromHorizontal)
//            transposeBack();

        if (width() <= 1 )
            throw new IllegalArgumentException("the width of the picture is less than or equal to 1");

        if (seam.length != height())
            throw new IllegalArgumentException("the height of the picture does not match to the length of seam. " + seam.length + ":" + height());

        if (seam[0] < 0 || seam[0] > width() - 1)
            throw new IllegalArgumentException("two adjacent entries differ by more than 1");

        for (int i = 1; i < seam.length; ++i) {
            if (seam[i] < 0 || seam[i] > width() - 1)
                throw new IllegalArgumentException(seam[i] + " lies outside of the picture");

            if ((Math.abs(seam[i] - seam[i - 1]) > 1))
                throw new IllegalArgumentException(
                        seam[i] + " and " + seam[i - 1] + ", two adjacent entries differ by more than 1");
        }

        // StdOut.println("removeVerticalSeam: h:" + height()+", w:"+width());
         int[][] afterPixels = new int[height()][width() - 1];

        for (int row = 0; row < seam.length; ++row) {
            // StdOut.println("seam[row]: " + seam[row]);
             System.arraycopy(picturePixels[row], 0, afterPixels[row], 0, seam[row]);
            System.arraycopy(picturePixels[row], seam[row] + 1, afterPixels[row], seam[row],
                    width() - (seam[row] + 1));
        }

         this.setPicturePixels(afterPixels);
//        if(fromHorizontal)
//            height = height() - 1;
//        else
//            width = width() - 1;
    }
}