import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {
    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        final int alphabets[] = new int[256];
        final int index[] = new int[256];
        for (int i = 0; i < alphabets.length; ++i) {
            alphabets[i] = i;
            index[i] = i;
        }
        
        while (!BinaryStdIn.isEmpty()) {
            char c = BinaryStdIn.readChar();
            int i = alphabets[c];
            BinaryStdOut.write(i, 8);
            for(int j=0; j<i; ++j) {
                ++alphabets[index[j]];
            }
            alphabets[c] = 0;
            System.arraycopy(index, 0, index, 1, i);
            index[0] = c;
        }
        
        BinaryStdOut.flush();
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        final int alphabets[] = new int[256];
        final int index[] = new int[256];
        for (int i = 0; i < alphabets.length; ++i) {
            alphabets[i] = i;
            index[i] = i;
        }

        while (!BinaryStdIn.isEmpty()) {
            char c = BinaryStdIn.readChar();
            BinaryStdOut.write(index[c], 8);
            c = (char) index[c];
            int i = alphabets[c];
            for(int j=0; j<i; ++j) {
                ++alphabets[index[j]];
            }
            alphabets[c] = 0;
            System.arraycopy(index, 0, index, 1, i);
            index[0] = c;
        }

        BinaryStdOut.flush();
        BinaryStdOut.close();        
    }

    // if args[0] is '-', apply move-to-front encoding
    // if args[0] is '+', apply move-to-front decoding
    public static void main(String[] args) {
        boolean encoding = "-".trim().equals(args[0]);
        
        if(encoding)
            encode();
        else
            decode();
    }
}
