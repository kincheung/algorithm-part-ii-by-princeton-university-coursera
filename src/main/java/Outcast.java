public class Outcast {
    private WordNet wordnet;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        int max = Integer.MIN_VALUE;
        String outcast = null;
        int sumD = 0;
        for (String nounA : nouns) {
            sumD = 0;
            for (String nounB : nouns) {
                if (!nounA.equals(nounB)) {
                    sumD += wordnet.distance(nounA, nounB);
//                     System.out.println(nounA + " " + nounB + ": " + sumD);
                }
            }

//             if("bed".equals(nounA))
//             System.out.println(nounA + " " + sumD);
            if (max < sumD) {
                max = sumD;
                outcast = nounA;
            }
        }

        return outcast;
    }

    public static void main(String[] args) { // see test client below
    }
}