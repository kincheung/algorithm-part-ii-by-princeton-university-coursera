import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class BurrowsWheeler {
    // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
    public static void encode() {
        StringBuilder sb = new StringBuilder();
        while (!BinaryStdIn.isEmpty()) {
            sb.append(BinaryStdIn.readChar());  
        }
        String s = sb.toString();
        CircularSuffixArray csa = new CircularSuffixArray(sb.toString());
        
        int first = -1;
        for (int i = 0; i < csa.length(); ++i) {
            if(csa.index(i) == 0) {
                first = i;
                break;
            }
        }
        
        BinaryStdOut.write(first);

        int N = csa.length();
        for (int i = 0; i < N; ++i) {
            BinaryStdOut.write(s.charAt((csa.index(i) + N -1) %N));
        }
        
        BinaryStdOut.flush();
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
    public static void decode() {
        int first = -1;
        if (!BinaryStdIn.isEmpty()) {
            first = BinaryStdIn.readInt();  
        }
//        System.err.println("first " + first);
        StringBuilder sb = new StringBuilder();
        while (!BinaryStdIn.isEmpty()) {
            sb.append(BinaryStdIn.readChar());  
        }
        char[] t = sb.toString().toCharArray();

//        for (int i = 0; i < t.length; ++i) {
//            System.err.print(t[i]);
//        }
//        System.err.println();
        
        int N = t.length;
        int R = 256;   // extend ASCII alphabet size
        char[] s = new char[N];
        

        // compute frequency counts
        int[] count = new int[R+1];
        int[] firstIndex = new int[R];
        for (int i = 0; i < N; i++) {
            count[t[i] + 1]++;
            if (firstIndex[t[i]] == 0)
                firstIndex[t[i]] = i + 1;
        }

        // compute cumulates
        for (int r = 0; r < R; r++)
            count[r+1] += count[r];

        // move data
        for (int i = 0; i < N; i++)
            s[count[t[i]]++] = t[i];
        
        
        int[] next = new int[t.length];
        for (int i = 0; i < s.length-1; ++i) {
            char c = s[i];
            char nextone = s[i+1];
            next[i] = firstIndex[c] - 1;
            
            if(i == 0 || nextone == c) {
                int k = next[i] + 1;
                for(; k < t.length; ++k) {
                    if(t[k] == c){
                        firstIndex[c] = k + 1;
                        break;
//                        count[c]--;
                    }
                }
            }
        }
        next[s.length-1] = firstIndex[s[s.length-1]]-1;

//        for (int i = 0; i < s.length; ++i) {
//            System.err.print(s[i]);
//        }
//        System.err.println();
        

//        for (int i = 0; i < next.length; ++i) {
//            System.err.print(next[i] + " ");
//        }
//        System.err.println();

        BinaryStdOut.write(s[first]);
        int j = first;
        for (int i = 0; i < next.length-1; ++i) {
            BinaryStdOut.write(s[next[j]]);
            j = next[j];
        }

        BinaryStdOut.flush();
        BinaryStdOut.close();
    }

    // if args[0] is '-', apply Burrows-Wheeler encoding
    // if args[0] is '+', apply Burrows-Wheeler decoding
    public static void main(String[] args) {
        boolean encoding = "-".trim().equals(args[0]);
        
        if(encoding)
            encode();
        else
            decode();
    }
}