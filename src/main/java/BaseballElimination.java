import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class BaseballElimination {
    private Map<String, Team> teams;
    private int[][] g;
    private int matches;
    private Map<String, List<String>> certOfEliminations;

    private static class Team {
        private int index;
        private int win;
        private int lose;
        private int remaining;

        public Team(int index, int win, int lose, int remaining) {
            super();
            this.index = index;
            this.win = win;
            this.lose = lose;
            this.remaining = remaining;
        }

        public int getIndex() {
            return index;
        }

        public int getWins() {
            return win;
        }

        public int getLosses() {
            return lose;
        }

        public int getRemaining() {
            return remaining;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Team [index=").append(index).append(", win=").append(win)
                    .append(", lose=").append(lose).append(", remaining=").append(remaining).append("]");
            return builder.toString();
        }

    }

    private int getMatches(int n) {
        return (n * (n - 1)) / 2; // n choose 2
    }

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {
        In in = new In(filename);

        int n = 0;
        if (!in.isEmpty()) {
            n = in.readInt();
        }

        teams = new LinkedHashMap<String, Team>(n);
        g = new int[n][n];
        int i = 0;

        while (!in.isEmpty()) {
            String name = in.readString();
            int w = in.readInt(), l = in.readInt(), r = in.readInt();

            Team team = new Team(i, w, l, r);
            for (int k = 0; k < n; ++k) {
                g[i][k] = in.readInt();
            }
            teams.put(name, team);
            ++i;
        }

        matches = getMatches(n);
        certOfEliminations = new HashMap<String, List<String>>();
        // System.out.println("Total matches: " + matches);

        // for (Team t : teams.values()) {
        // System.out.println(t);
        // }
    }

    // number of teams
    public int numberOfTeams() {
        return teams.size();
    }

    // all teams
    public Iterable<String> teams() {
        return Collections.unmodifiableSet(teams.keySet());
    }

    // number of wins for given team
    public int wins(String team) {
        if (!teams.containsKey(team))
            throw new IllegalArgumentException();
        
        return teams.get(team).getWins();
    }

    // number of losses for given team
    public int losses(String team) {
        if (!teams.containsKey(team))
            throw new IllegalArgumentException();
        return teams.get(team).getLosses();
    }

    // number of remaining games for given team
    public int remaining(String team) {
        if (!teams.containsKey(team))
            throw new IllegalArgumentException();
        return teams.get(team).getRemaining();
    }

    // number of remaining games between team1 and team2
    public int against(String team1, String team2) {
        if (!teams.containsKey(team1) || !teams.containsKey(team2))
            throw new IllegalArgumentException();
        return g[teams.get(team1).getIndex()][teams.get(team2).getIndex()];
    }

    // is given team eliminated?
    public boolean isEliminated(String team) {
        if (!teams.containsKey(team))
            throw new IllegalArgumentException();
        
        if(certOfEliminations.containsKey(team)) {
            return !certOfEliminations.get(team).isEmpty();
        }

        // Trivial elimination
        List<String> coe = trivialElimination(team);
        certOfEliminations.put(team, coe);

        // maxFlow elimination
        if (coe.isEmpty()) {
            coe = maxFlowElimination(team);
            certOfEliminations.put(team, coe);
        }


        return !coe.isEmpty();
    }

    private List<String>  maxFlowElimination(String team) {
        List<String> certOfEliminations = new LinkedList<String>();
        int V = 1 + matches + numberOfTeams() + 1;
        // int E = totalRemainingGames + (totalRemainingGames * numberOfTeams() * 2) + numberOfTeams();
        // System.out.println("V = " + V);
        FlowNetwork fn = new FlowNetwork(V);

        int src = 0;
        int gameVertex = src + 1;
        int teamVertex = gameVertex + matches;
        int numVFromSrcAndGameV = matches + 1;
        int sink = V - 1;
        String[] a = teams.keySet().toArray(new String[0]);

        for (int i = 0; i < a.length; ++i) {
            for (int j = i + 1; j < a.length; ++j) {
                // StdOut.println(i + " vs " + j);
                fn.addEdge(new FlowEdge(src, gameVertex, this.against(a[i], a[j])));
                fn.addEdge(new FlowEdge(gameVertex, teamVertex + teams.get(a[i]).getIndex(), Double.POSITIVE_INFINITY));
                fn.addEdge(new FlowEdge(gameVertex, teamVertex + teams.get(a[j]).getIndex(), Double.POSITIVE_INFINITY));
                ++gameVertex;
            }

            fn.addEdge(new FlowEdge(teamVertex + teams.get(a[i]).getIndex(), sink,
                    this.wins(team) + this.remaining(team) - this.wins(a[i])));
        }

        // System.out.println("fn: " + fn);

        FordFulkerson ff = new FordFulkerson(fn, src, sink);
        for (String t : this.teams()) {
            if (ff.inCut(numVFromSrcAndGameV + teams.get(t).getIndex())) {
                certOfEliminations.add(t);
            }
        }
        
        return certOfEliminations;
    }

    private List<String> trivialElimination(String team) {
        List<String> certOfEliminations = new LinkedList<String>();
        int maxGameWin = this.wins(team) + this.remaining(team);
        for (String teamName : teams()) {
            // System.out.println(maxGameWin + " vs " + this.wins(teamName));
            if (!team.equals(teamName) && maxGameWin < this.wins(teamName)) {
                certOfEliminations.add(teamName);
            }
        }
        return certOfEliminations;
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {
        if (!teams.containsKey(team))
            throw new IllegalArgumentException();
        
        if (!isEliminated(team))
            return null;
        else
            return Collections.unmodifiableList(certOfEliminations.get(team));
    }

    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            } else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }

}
