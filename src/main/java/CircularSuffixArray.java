import java.io.FileNotFoundException;

import edu.princeton.cs.algs4.In;

public class CircularSuffixArray {
    private static final int R = 256;
    private static final int CUTOFF        =  15;   // cutoff to insertion sort

    private int[] index;
    
    private int nextPos(int i, int d, int N) {
        return (i + d) % N;
    }
    
    private void sort(String s) {
        int N = s.length();
        char[] array = s.toCharArray();
        int[] aux = new int[N];

        index = new int[s.length()];
        for (int i = 0; i < N; i++) {
            index[i] = i;   
        }
        
        sort(array, 0, N-1, 0, aux);
    }
    
    // insertion sort a[lo..hi], starting at dth character
    private void insertion(char[] a, int lo, int hi, int d) {
        for (int i = lo; i <= hi; i++)
            for (int j = i; j > lo && less(a, index[j], index[j-1], d); j--)
                exch(a, j, j-1);
    }

    // exchange a[i] and a[j]
    private void exch(char[] a, int i, int j) {
        int temp = index[i];
        index[i] = index[j];
        index[j] = temp;
    }
    
    // is v less than w, starting at character d
    private boolean less(char[] a, int v, int w, int d) {
        int N = a.length;
        for (int i = d; i < N; i++) {
            if (a[(v + i) % N] < a[(w + i) % N]) return true;
            if (a[(v + i) % N] > a[(w + i) % N]) return false;
        }
        return false;
    }
    
    private void sort(char[] a, int lo, int hi, int d, int[] aux) {
        int N = a.length;        
        // cutoff to insertion sort for small subarrays
        if (N == d) {
            return;
        }
        
        if (hi <= lo + CUTOFF) {
            insertion(a, lo, hi, d);
            return;
        }

        // compute frequency counts
        int[] count = new int[R+2];
        for (int i = lo; i <= hi; i++) {
            int c = a[nextPos(index[i], d, N)];
            count[c+2]++;
        }

        // transform counts to indicies
        for (int r = 0; r < R+1; r++)
            count[r+1] += count[r];

        // distribute
        for (int i = lo; i <= hi; i++) {
            int c = a[nextPos(index[i], d, N)];
            aux[count[c+1]++] = index[i];
        }

        // copy back
        for (int i = lo; i <= hi; i++) 
            index[i] = aux[i - lo];


        // recursively sort for each character (excludes sentinel -1)
        for (int r = 0; r < R; r++)
            if((lo + count[r+1] - 1) - (lo + count[r]) > 0) {
                sort(a, lo + count[r], lo + count[r+1] - 1, d + 1, aux);
            }
        
    }
    
    // circular suffix array of s
    public CircularSuffixArray(String s) {
        sort(s);
    }

    // length of s
    public int length() {
        return index.length;
    }

    // returns index of ith sorted suffix
    public int index(int i) {
        return index[i];
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) throws FileNotFoundException {
        In in = new In("burrows\\dickens.txt");
        String txt = in.readAll();

        long start = 0;
        long accu = 0;
        final int N = 20;
        String partial = txt.substring(0, 256000);
        for(int i=0; i<N; ++i) {
            start = System.currentTimeMillis();
            new CircularSuffixArray(partial);
            accu += (System.currentTimeMillis() - start);
        }
        System.err.println((accu / N) + "ms");

//        CircularSuffixArray c = new CircularSuffixArray("ABRACADABRA!");
//        for(int i=0; i<c.length(); ++i){
//            System.err.print(c.index(i) + " ");
//        }            
//        System.err.println();
                
    }
}
