import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;

public class WordNet {
    private Map<Integer, Synset> synsetMap; // ID to Synset
    private Map<String, List<Integer>> nounSynsetMap;
    private SAP sap;

    private static class Synset {
        // public String id;
        private String synset;
        // public String gloss;

        public Synset(String synset) {
            // this.id = id;
            this.synset = synset;
            // this.gloss = gloss;
        }

        public String toString() {
            return "synset: " + synset;
        }
    }

    // constructor takes the name of the two input files
    // nlogn
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) {
            throw new NullPointerException();
        }

        populateSynsets(synsets);
        makeRootedDAG(hypernyms);
    }

    private void makeRootedDAG(String hypernyms) {
        Digraph digraph = new Digraph(synsetMap.size()); // V = |synsets|

        In in2 = new In(hypernyms);
        while (!in2.isEmpty()) {
            String s = in2.readLine();
            String[] tokens = s.split(",");
            for (int i = 1; i < tokens.length; i++) {
                digraph.addEdge(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[i]));
            }
        }

        // System.out.println(String.format("digraph: E:%d, V:%d", digraph.E(), digraph.V()));

        checkRootedDAG(digraph);

        sap = new SAP(digraph);
    }

    private void populateSynsets(String synsets) {
        In in = null;
        in = new In(synsets);

        nounSynsetMap = new HashMap<String, List<Integer>>();
        synsetMap = new HashMap<Integer, Synset>();
        while (!in.isEmpty()) {
            String s = in.readLine();
            String[] tokens = s.split(",");
            Synset synset = new Synset(tokens[1]);
            synsetMap.put(Integer.parseInt(tokens[0]), synset);

            String[] nounTokens = tokens[1].split(" ");
            for (String noun : nounTokens) {
                if (!nounSynsetMap.containsKey(noun)) {
                    nounSynsetMap.put(noun, new ArrayList<Integer>());
                }
                nounSynsetMap.get(noun).add(Integer.parseInt(tokens[0]));
            }
        }
    }

    private void checkRootedDAG(Digraph digraph) {
        // check cycle
        DirectedCycle dc = new DirectedCycle(digraph);
        if (dc.hasCycle())
            throw new IllegalArgumentException("there is a cycle in the graph");

        // check single root
        int root = -1;
        for (int i = 0; i < digraph.V(); ++i) {
            if (digraph.outdegree(i) == 0) {
                if (root != -1)
                    throw new IllegalArgumentException("there are more than one root in the graph " + root + " and " + i);
                else
                    root = i;
            }
        }

        // check single ancestor
        BreadthFirstDirectedPaths dsp = new BreadthFirstDirectedPaths(digraph.reverse(), root);
        for (int i = 0; i < digraph.V(); ++i) {
            if (!dsp.hasPathTo(i)) {
                throw new IllegalArgumentException(
                        "there is a vertex whose ancestor is not the root in the graph. " + i + " to " + root);
            }
        }
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return nounSynsetMap.keySet();
    }

    // is the word a WordNet noun?
    // logn
    public boolean isNoun(String word) {
        if (word == null) {
            throw new NullPointerException();
        }

        return nounSynsetMap.containsKey(word);
    }

    // distance between nounA and nounB (defined below)
    // n
    public int distance(String nounA, String nounB) {
        if (nounA == null || nounB == null) {
            throw new NullPointerException();
        }

        Iterable<Integer> nounAId = nounSynsetMap.get(nounA);
        checkIsWordNetNoun(nounA, nounAId);
        Iterable<Integer> nounBId = nounSynsetMap.get(nounB);
        checkIsWordNetNoun(nounB, nounBId);

        return sap.length(nounAId, nounBId);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of
    // nounA and nounB
    // in a shortest ancestral path (defined below)
    // n
    public String sap(String nounA, String nounB) {
        if (nounA == null || nounB == null) {
            throw new NullPointerException();
        }
        Iterable<Integer> nounAId = nounSynsetMap.get(nounA);
        checkIsWordNetNoun(nounA, nounAId);
        Iterable<Integer> nounBId = nounSynsetMap.get(nounB);
        checkIsWordNetNoun(nounB, nounBId);

        return synsetMap.get(sap.ancestor(nounAId, nounBId)).synset;
    }

    private void checkIsWordNetNoun(String nounA, Iterable<Integer> nounAId) {
        if (nounAId == null) {
            throw new IllegalArgumentException(nounA + " is not a noun in WordNet");
        }
    }

    // do unit testing of this class
    public static void main(String[] args) {
    }
}