import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;

public class SAP_ACE {
    private static final int NOT_FOUND = -1;
    private final Digraph G;
    private final Map<String, SAPData> queryCache = new HashMap<String, SAPData>();
    private final Map<String, Map<Integer, Integer>> pathCache = new HashMap<String, Map<Integer, Integer>>();
    private final Map<String, Queue<Integer>> terminatedPoint = new HashMap<String, Queue<Integer>>();

    private static class SAPData {
        private final int length;
        private final int ancestor;

        public SAPData(int length, int ancestor) {
            super();
            this.length = length;
            this.ancestor = ancestor;
        }

        public int getLength() {
            return length;
        }

        public int getAncestor() {
            return ancestor;
        }
    }

    private class MyBSF {
        private final Digraph graph;
        private final Map<Integer, Integer> distTo;
        private final Queue<Integer> queue;
        private boolean terminate;
        private final String name;
        private final boolean recorded;

        public MyBSF(String name, Digraph graph, int source) {
            this.name = name;
            this.graph = graph;
//            if (pathCache.containsKey(name)) {
//                this.distTo = pathCache.get(name);
//                if (terminatedPoint.containsKey(name)) {
//                    queue = terminatedPoint.get(name);
//                } else {
//                    queue = new Queue<Integer>();
//                }
//                recorded = true;
//            } else {
                this.distTo = new HashMap<Integer, Integer>();
                pathCache.put(name, distTo);
                queue = new Queue<Integer>();
                queue.enqueue(source);
                distTo.put(source, 0);
                recorded = false;
//            }
        }

        public MyBSF(String name, Digraph graph, Iterable<Integer> sources) {
            this.name = name;
            this.graph = graph;

//            if (pathCache.containsKey(name)) {
//                this.distTo = pathCache.get(name);
//                if (terminatedPoint.containsKey(name)) {
//                    queue = terminatedPoint.get(name);
//                } else {
//                    queue = new Queue<Integer>();
//                }
//                recorded = true;
//            } else {
                this.distTo = new HashMap<Integer, Integer>();
                pathCache.put(name, distTo);
                queue = new Queue<Integer>();
                for (int source : sources) {
                    queue.enqueue(source);
                    distTo.put(source, 0);
                }
                recorded = false;
//            }
        }

        public int[] next() {
            int[] adj = null;
            int i = 0;
            if (hasNext()) {
                int v = queue.dequeue();
                // if(this.isRecorded()) {
                // System.out.println(name + ": resumed from " + v);
                // }
                adj = new int[graph.outdegree(v)]; // no. of adj == outdegree
                for (int w : graph.adj(v)) {
                    adj[i++] = w;
                    // System.out.println(name + " -> v:" + v + " to " + w + " path: " + (distTo.get(v) + 1));
                    if (!distTo.containsKey(w)) {
                        distTo.put(w, distTo.get(v) + 1);
                        if (graph.outdegree(w) > 0) { // other than root
                            queue.enqueue(w);
                            terminatedPoint.remove(name); // reached the end
                        }
                    }
                }
            }

            return adj;
        }

        public int distTo(int w) {
            if (!visited(w)) {
                return NOT_FOUND;
            }

            return distTo.get(w);
        }

        public boolean visited(int w) {
            return distTo.containsKey(w);
        }

        public void terminate() {
            // System.out.println("terminate ");
            if (!queue.isEmpty()) {
                // System.out.println("terminated: " + name + " last: " + queue.peek());
                terminatedPoint.put(name, queue);
            } else {
                terminatedPoint.remove(name);
            }
            this.terminate = true;
        }

        public boolean hasNext() {
            return !terminate && !queue.isEmpty();
        }

        public boolean isRecorded() {
            return recorded;
        }

        public Set<Integer> getDistTo() {
            return this.distTo.keySet();
        }
    }

    // constructor takes a digraph (not necessarily a DAG)
    public SAP_ACE(Digraph G) {
        this.G = new Digraph(G);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getLength();
        } else {
            key = toKey(w, v);
            sapData = queryCache.get(key);
            if (sapData != null) {
                return sapData.getLength();
            }
        }
        return helper(v, w, 0);
    }

    // a common ancestor of v and w that participates in a shortest ancestral
    // path; -1 if no such path
    public int ancestor(int v, int w) {
        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getAncestor();
        } else {
            key = toKey(w, v);
            sapData = queryCache.get(key);
            if (sapData != null) {
                return sapData.getAncestor();
            }
        }
        return helper(v, w, 1);
    }

    // length of shortest ancestral path between any vertex in v and any vertex
    // in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getLength();
        } else {
            key = toKey(w, v);
            sapData = queryCache.get(key);
            if (sapData != null) {
                return sapData.getLength();
            }
        }
        return helper(v, w, 0);
    }

    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getAncestor();
        } else {
            key = toKey(w, v);
            sapData = queryCache.get(key);
            if (sapData != null) {
                return sapData.getAncestor();
            }
        }
        return helper(v, w, 1);
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no
    // such path
    private int helper(Iterable<Integer> v, Iterable<Integer> w, int caller) {
        SAPData sapData = null;

        MyBSF vBsf = new MyBSF(toKey(v), this.G, v);
        MyBSF wBsf = new MyBSF(toKey(w), this.G, w);

        if (wBsf.isRecorded() && vBsf.isRecorded()) {
            MyBSF temp = null;

            // System.out.println(vBsf.getDistTo().size() + " vs " + wBsf.getDistTo().size());
            if (vBsf.getDistTo().size() > wBsf.getDistTo().size()) {
                temp = vBsf;
                vBsf = wBsf;
                wBsf = temp;
            }
        }

        for (int vv : vBsf.getDistTo()) {
            if (wBsf.visited(vv)) {
                if (sapData == null || sapData.getLength() > (wBsf.distTo(vv) + vBsf.distTo(vv))) {
                    sapData = new SAPData(wBsf.distTo(vv) + vBsf.distTo(vv), vv);
                    break;
                }
            }
        }

        if (sapData == null) {
            sapData = lockstep(vBsf, wBsf);
        }

        if (sapData != null) {
            cacheSAPData(v, w, sapData);
            if (caller == 1)
                return sapData.getAncestor();
            else
                return sapData.getLength();
        } else {
            cacheSAPData(v, w, new SAPData(NOT_FOUND, NOT_FOUND));
            return NOT_FOUND;
        }
    }

    private int helper(int v, int w, int caller) {
        SAPData sapData = null;

        MyBSF vBsf = new MyBSF(toKey(v), this.G, v);
        MyBSF wBsf = new MyBSF(toKey(w), this.G, w);

        for (int vv : vBsf.getDistTo()) {
            if (wBsf.visited(vv)) {
                if (sapData == null || sapData.getLength() > (wBsf.distTo(vv) + vBsf.distTo(vv))) {
                    sapData = new SAPData(wBsf.distTo(vv) + vBsf.distTo(vv), vv);
                }
            }
        }

        // if (wBsf.visited(v) || vBsf.visited(w)) {
        // sapData = new SAPData(wBsf.distTo(v) + vBsf.distTo(v), v); // case when distance is 0, SAP is source
        // }

        if (sapData == null) {
            sapData = lockstep(vBsf, wBsf);
        }

        if (sapData != null) {
            cacheSAPData(v, w, sapData);
            if (caller == 1)
                return sapData.getAncestor();
            else
                return sapData.getLength();
        } else {
            cacheSAPData(v, w, new SAPData(NOT_FOUND, NOT_FOUND));
            return NOT_FOUND;
        }
    }

    private SAPData lockstep(MyBSF vBsf, MyBSF wBsf) {
        SAPData sapData = null;

        int[] vAdj = null;
        int[] wAdj = null;
        // walk both sides
        while (vBsf.hasNext() || wBsf.hasNext()) {
            vAdj = vBsf.next();
            wAdj = wBsf.next();

            if (vAdj != null) {
                for (int i = 0; i < vAdj.length; ++i) {
                    int b = vAdj[i];
                    if (wBsf.visited(b)) {
                        int length = vBsf.distTo(b) + wBsf.distTo(b);
                        if (sapData == null || sapData.getLength() > length) {
                            sapData = new SAPData(length, b);
                        }
                        if (sapData == null || vBsf.distTo(b) >= sapData.getLength()) {
                            vBsf.terminate();
                            break;
                        }
                    }
                }
            } 

            if (wAdj != null) {
                for (int i = 0; i < wAdj.length; ++i) {
                    int b = wAdj[i];
                    if (vBsf.visited(b)) {
                        int length = vBsf.distTo(b) + wBsf.distTo(b);
                        if (sapData == null || sapData.getLength() > length) {
                            sapData = new SAPData(length, b);
                        }
                        if (sapData == null || wBsf.distTo(b) >= sapData.getLength()) {
                            wBsf.terminate();
                            break;
                        }
                    }
                }
            }
            // else {
            // System.out.println("w ended");
            // }
        }
        return sapData;
    }

    private void cacheSAPData(int v, int w, SAPData sapData) {
        String key = toKey(v, w);
        queryCache.put(key, sapData);
    }

    private void cacheSAPData(Iterable<Integer> v, Iterable<Integer> w, SAPData sapData) {
        String key = toKey(v, w);
        queryCache.put(key, sapData);
    }

    private String toKey(Iterable<Integer> v, Iterable<Integer> w) {
        StringJoiner sa = new StringJoiner(",");
        for (int vv : v) {
            sa.add(String.valueOf(vv));
        }
        sa.add("");
        for (int ww : w) {
            sa.add(String.valueOf(ww));
        }

        return sa.toString();
    }

    private String toKey(Iterable<Integer> v) {
        StringJoiner sa = new StringJoiner(",");
        for (int vv : v) {
            sa.add(String.valueOf(vv));
        }

        return sa.toString();
    }

    private String toKey(int v, int w) {
        return v + ":" + w;
    }

    private String toKey(int v) {
        return String.valueOf(v);
    }

    // do unit testing of this class
    public static void main(String[] args) {
    }
}