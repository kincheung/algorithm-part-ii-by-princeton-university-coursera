//package backup;
//import edu.princeton.cs.algs4.DirectedEdge;
//import edu.princeton.cs.algs4.EdgeWeightedDigraph;
//import edu.princeton.cs.algs4.IndexMinPQ;
//
//public class DijkstraSP {
//    private DirectedEdge[] edgeTo;
//    private double[] distTo;
//    private IndexMinPQ<Double> pq;
//    
//    private static final char[] KEYS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
//
//    public DijkstraSP(EdgeWeightedDigraph G, int s) {
//        edgeTo = new DirectedEdge[G.V()];
//        distTo = new double[G.V()];
//        pq = new IndexMinPQ<Double>(G.V());
//        for (int v = 0; v < G.V(); v++)
//            distTo[v] = Double.POSITIVE_INFINITY;
//        distTo[s] = 0.0;
//        pq.insert(s, 0.0);
//        while (!pq.isEmpty()) {
//            int v = pq.delMin();
//            for (DirectedEdge e : G.adj(v)) {
//                relax(e);
//            }
//            if (v == 4) {
//                for (int i=0; i<distTo.length; ++i) {
//                    System.out.println(KEYS[i] + ": " + distTo[i]);
//                }
//                return;
//            }
//        }
//    }
//
//    private void relax(DirectedEdge e) {
//        int v = e.from(), w = e.to();
//        if (distTo[w] > distTo[v] + e.weight()) {
//            distTo[w] = distTo[v] + e.weight();
//            edgeTo[w] = e;
//            if (pq.contains(w))
//                pq.decreaseKey(w, distTo[w]);
//            else
//                pq.insert(w, distTo[w]);
//        }
//    }
//    
//    //  A   B   C   D   E   F   G   H 
//    //  0   1   2   3   4   5   6   7
//    //    A->F     2
//    //    B->A     6
//    //    C->B    36
//    //    C->D    35
//    //    E->A    16
//    //    F->B    34
//    //    F->E    32
//    //    G->B    54
//    //    G->C    15
//    //    G->D    54
//    //    G->F    13
//    //    H->D    62
//    //    H->G     3
//    public static void main(String args[]) {
//        EdgeWeightedDigraph g = new EdgeWeightedDigraph(8);
//        g.addEdge(new DirectedEdge(0, 5, 2));
//        g.addEdge(new DirectedEdge(1, 0, 6));
//        g.addEdge(new DirectedEdge(2, 1, 36));
//        g.addEdge(new DirectedEdge(2, 3, 35));
//        g.addEdge(new DirectedEdge(4, 0, 16));
//        g.addEdge(new DirectedEdge(5, 1, 34));
//        g.addEdge(new DirectedEdge(5, 4, 32));
//        g.addEdge(new DirectedEdge(6, 1, 54));
//        g.addEdge(new DirectedEdge(6, 2, 15));
//        g.addEdge(new DirectedEdge(6, 3, 54));
//        g.addEdge(new DirectedEdge(6, 5, 13));
//        g.addEdge(new DirectedEdge(7, 3, 62));
//        g.addEdge(new DirectedEdge(7, 6, 3));
//        
//        DijkstraSP d = new DijkstraSP(g, 7); //D
//    }
//}
