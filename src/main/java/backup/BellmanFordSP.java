package backup;
import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.EdgeWeightedDigraph;

public class BellmanFordSP {
    private DirectedEdge[] edgeTo;
    private double[] distTo;
    private static final char[] KEYS = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };

    // G H B C D A F E
    public BellmanFordSP(EdgeWeightedDigraph G, int s) {
        edgeTo = new DirectedEdge[G.V()];
        distTo = new double[G.V()];
        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[s] = 0.0;
        
        for (int i = 0; i < 3; i++){
            for (int v = 0; v < G.V(); v++){
                for (DirectedEdge e : G.adj(v)) {
                    relax(e);
                }
            }
        }


        for (int i = 0; i < distTo.length; ++i) {
            System.out.println(KEYS[i] + ": " + distTo[i]);
        }
    }

    private void relax(DirectedEdge e) {
        int v = e.from(), w = e.to();
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight();
            edgeTo[w] = e;
        }
    }

    // A B C D E F G H
    // 0 1 2 3 4 5 6 7
//        A->E    35
//        B->A    24
//        C->F    88
//        C->B    19
//        C->G    29
//        D->H    34
//        D->C     9
//        E->F     3
//        F->G     1
//        F->A     3
//        F->B     3
//        H->C     3
//        H->G     4
    public static void main(String[] args) {
        EdgeWeightedDigraph g = new EdgeWeightedDigraph(8);
        g.addEdge(new DirectedEdge(0, 4, 35));
        g.addEdge(new DirectedEdge(1, 0, 24));
        g.addEdge(new DirectedEdge(2, 5, 88));
        g.addEdge(new DirectedEdge(2, 1, 19));
        g.addEdge(new DirectedEdge(2, 6, 29));
        g.addEdge(new DirectedEdge(3, 7, 34));
        g.addEdge(new DirectedEdge(3, 2, 9));
        g.addEdge(new DirectedEdge(4, 5, 3));
        g.addEdge(new DirectedEdge(5, 6, 1));
        g.addEdge(new DirectedEdge(5, 0, 3));
        g.addEdge(new DirectedEdge(5, 1, 3));
        g.addEdge(new DirectedEdge(7, 2, 3));
        g.addEdge(new DirectedEdge(7, 6, 4));

        BellmanFordSP d = new BellmanFordSP(g, 3); // D
    }

}
