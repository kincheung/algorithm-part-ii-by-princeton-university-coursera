package backup;
import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.EdgeWeightedDigraph;
import edu.princeton.cs.algs4.Topological;

public class AcyclicSP {
    private DirectedEdge[] edgeTo;
    private double[] distTo;
    private static final char[] KEYS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

    // G H B C D A F E 
    public AcyclicSP(EdgeWeightedDigraph G, int s) {
        edgeTo = new DirectedEdge[G.V()];
        distTo = new double[G.V()];
        for (int v = 0; v < G.V(); v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[s] = 0.0;
        Topological topological = new Topological(G);
        for (int v : topological.order()) {
            for (DirectedEdge e : G.adj(v)) {
                relax(e);
            }
            if (v == 0) { //A
                for (int i=0; i<distTo.length; ++i) {
                    System.out.println(KEYS[i] + ": " + distTo[i]);
                }
                return;
            }
        }
    }

    private void relax(DirectedEdge e) {
        int v = e.from(), w = e.to();
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight();
            edgeTo[w] = e;
        }
    }
    
    //  A   B   C   D   E   F   G   H 
    //  0   1   2   3   4   5   6   7
    //    A->E     9
    //    A->F     3
    //    B->A    36
    //    B->F    23
    //    C->B     3
    //    D->C    21
    //    F->E    13
    //    G->B    34
    //    G->C    76
    //    G->D    48
    //    G->F    60
    //    G->H    28
    //    H->D    17
    public static void main(String[] args) {
        EdgeWeightedDigraph g = new EdgeWeightedDigraph(8);
        g.addEdge(new DirectedEdge(0, 4, 9));
        g.addEdge(new DirectedEdge(0, 5, 3));
        g.addEdge(new DirectedEdge(1, 0, 36));
        g.addEdge(new DirectedEdge(1, 5, 23));
        g.addEdge(new DirectedEdge(2, 1, 3));
        g.addEdge(new DirectedEdge(3, 2, 21));
        g.addEdge(new DirectedEdge(5, 4, 13));
        g.addEdge(new DirectedEdge(6, 1, 34));
        g.addEdge(new DirectedEdge(6, 2, 76));
        g.addEdge(new DirectedEdge(6, 3, 48));
        g.addEdge(new DirectedEdge(6, 5, 60));
        g.addEdge(new DirectedEdge(6, 7, 28));
        g.addEdge(new DirectedEdge(7, 3, 17));

        
        AcyclicSP d = new AcyclicSP(g, 6); //D
    }

}
