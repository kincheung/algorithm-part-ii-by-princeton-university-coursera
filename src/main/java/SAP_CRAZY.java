import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;

public class SAP_CRAZY {
    private static final int NOT_FOUND = -1;
    private Digraph G;
    private final Map<String, SAPData> queryCache = new HashMap<String, SAPData>();
    private final Map<String, Map<Integer, Integer>> pathCache = new HashMap<String, Map<Integer, Integer>>();
    private final Map<String, Queue<Integer>> terminatedPoint = new HashMap<String, Queue<Integer>>();
    private final Map<String, Integer> extendedMap = new HashMap<String, Integer>();
    private int extendedIndex;

    private static class SAPData {
        private final int length;
        private final int ancestor;

        public SAPData(int length, int ancestor) {
            super();
            this.length = length;
            this.ancestor = ancestor;
        }

        public int getLength() {
            return length;
        }

        public int getAncestor() {
            return ancestor;
        }
    }

    private class MyBSF {
        private final Digraph graph;
        private final Map<Integer, Integer> distTo;
        private final Queue<Integer> queue;
        private boolean terminated;
        private final String name;
        // private final boolean recorded;

        public MyBSF(String name, Digraph graph, int source) {
            this.name = name;
            this.graph = graph;
            Map<Integer, Integer> tdistTo = pathCache.get(name);
            if (tdistTo != null) {
                this.distTo = tdistTo;
                Queue<Integer> tqueue = terminatedPoint.get(name);
                if (tqueue != null) {
                    queue = tqueue;
                } else {
                    queue = null;
                    terminated = true;
                }
                // recorded = true;
                // System.out.println(name + ": got cache, starting at " + queue);
            } else {
                if (G.outdegree(source) > 0) {// can trigger indexOutOfBounds
                    queue = new Queue<Integer>();
                    queue.enqueue(source);
                } else {
                    queue = null;
                }
                this.distTo = new HashMap<Integer, Integer>();
                pathCache.put(name, distTo);
                distTo.put(source, 0);
                // recorded = false;
            }
        }

        // public MyBSF(String name, Digraph graph, Iterable<Integer> sources) {
        // this.name = name;
        // this.graph = graph;
        // Map<Integer, Integer> tdistTo = pathCache.get(name);
        // if (tdistTo != null) {
        // this.distTo = tdistTo;
        // Queue<Integer> tqueue = terminatedPoint.get(name);
        // if (tqueue != null) {
        // queue = tqueue;
        // } else {
        // queue = null;
        // terminated = true;
        // }
        // // System.out.println(name + ": got cache, starting at " + queue.peek());
        //// recorded = true;
        // } else {
        // this.distTo = new HashMap<Integer, Integer>();
        // pathCache.put(name, distTo);
        // queue = new Queue<Integer>();
        // sources.forEach((source) -> {
        // queue.enqueue(source);
        // distTo.put(source, 0);
        // });
        //// recorded = false;
        // }
        // }

        public int[] next() {
            int[] adj = null;
            int i = 0;
            if (hasNext()) {
                int v = queue.dequeue();
                // if(this.isRecorded()) {
                // System.out.println(name + ": resumed from " + v);
                // }
                adj = new int[graph.outdegree(v)]; // no. of adj == outdegree
                for (int w : graph.adj(v)) {
                    adj[i++] = w;
                    // System.out.println(name + " -> v:" + v + " to " + w + " path: " + (distTo.get(v) + 1));
                    if (!distTo.containsKey(w)) {
                        distTo.put(w, distTo.get(v) + 1);
                        if (graph.outdegree(w) > 0) { // other than root
                            queue.enqueue(w);
                            terminatedPoint.remove(name); // reached the end
                        }
                    }
                }
            }

            return adj;
        }

        public int distTo(int w) {
            if (!visited(w)) {
                return NOT_FOUND;
            }

            return distTo.get(w);
        }

        public boolean visited(int w) {
            return distTo.containsKey(w);
        }

        public boolean isTerminated() {
            return terminated;
        }

        public void terminate() {
            if (queue != null) {
                // System.out.println("terminate ");
                if (!queue.isEmpty()) {
                    // System.out.println("terminated: " + name + " last: " + queue.peek());
                    terminatedPoint.put(name, queue);
                } else {
                    // System.out.println("terminated: " + name + " remove");
                    terminatedPoint.remove(name);
                }
                this.terminated = true;
            }
        }

        public boolean hasNext() {
            return !terminated && queue != null && !queue.isEmpty();
        }

        // public boolean isRecorded() {
        // return recorded;
        // }

        public Set<Integer> getDistTo() {
            return this.distTo.keySet();
        }
    }

    private class MyDigraph extends Digraph {

        @SuppressWarnings("unchecked")
        public MyDigraph(Digraph G, int size) {
            super(size);
            try {
                Field E = Digraph.class.getDeclaredField("E");
                E.setAccessible(true);
                Field V = Digraph.class.getDeclaredField("V");
                V.setAccessible(true);
                Field indegree = Digraph.class.getDeclaredField("indegree");
                indegree.setAccessible(true);
                Field adj = Digraph.class.getDeclaredField("adj");
                adj.setAccessible(true);

                E.set(this, G.E());
                for (int v = 0; v < G.V(); v++)
                    ((int[]) indegree.get(this))[v] = G.indegree(v);
                for (int v = 0; v < G.V(); v++) {
                    // reverse so that adjacency list is in same order as original
                    Stack<Integer> reverse = new Stack<Integer>();
                    for (int w : G.adj(v)) {
                        reverse.push(w);
                    }
                    for (int w : reverse) {
                        ((Bag<Integer>[]) adj.get(this))[v].add(w);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    // constructor takes a digraph (not necessarily a DAG)
    public SAP_CRAZY(Digraph G) {
        int size = 16;
        if ( G.V() * 2 > 16)
            size =  G.V() * 2;
        
        this.G = new MyDigraph(G,size);
        extendedIndex = G.V();
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        return length(v, w, false);
    }

    private int length(int v, int w, boolean overdrive) {
        // if (!overdrive) {
        // if (v < 0 || v >= this.G.V() || w < 0 || w >= this.G.V())
        // throw new IndexOutOfBoundsException();
        // }

        if (v == w)
            return 0;

        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getLength();
        }
        // else {
        // key = toKey(w, v);
        // sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getLength();
        // }
        // }
        return helper(key, v, w, 0);
    }

    // a common ancestor of v and w that participates in a shortest ancestral
    // path; -1 if no such path
    public int ancestor(int v, int w) {
        return ancestor(v, w, false);
    }

    private int ancestor(int v, int w, boolean overdrive) {
        // if (!overdrive) {
        // if (v < 0 || v >= this.G.V() || w < 0 || w >= this.G.V())
        // throw new IndexOutOfBoundsException();
        // }

        if (v == w)
            return v;

        String key = toKey(v, w);
        SAPData sapData = queryCache.get(key);
        if (sapData != null) {
            return sapData.getAncestor();
        }
        // else {
        // key = toKey(w, v);
        // sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getAncestor();
        // }
        // }
        return helper(key, v, w, 1);
    }

    // length of shortest ancestral path between any vertex in v and any vertex
    // in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        String vKey = toKey(v), wKey = toKey(w);

        if (vKey.equals(wKey)) {
            if (v.iterator().hasNext() && w.iterator().hasNext())
                return 0;
            else
                return -1;
        }

        // String key = vKey + "+" + wKey;
        Integer extendedVIndex = extendedMap.get(vKey);
        if (extendedVIndex == null) {
            extendedVIndex = ++this.extendedIndex;
            for (int vv : v) {
                // if (vv < 0 || vv >= this.G.V())
                // throw new IndexOutOfBoundsException();

                if (extendedVIndex >= G.V()) { // graph doubling
                    G = new MyDigraph(G, G.V() * 2);
                }
                G.addEdge(extendedVIndex, vv);
            }
            extendedMap.put(vKey, extendedVIndex);
        }

        Integer extendedWIndex = extendedMap.get(wKey);
        if (extendedWIndex == null) {
            extendedWIndex = ++this.extendedIndex;
            for (int ww : w) {
                // if (ww < 0 || ww >= this.G.V())
                // throw new IndexOutOfBoundsException();
                if (extendedWIndex >= G.V()) { // graph doubling
                    G = new MyDigraph(G, G.V() * 2);
                }
                G.addEdge(extendedWIndex, ww);
            }
            extendedMap.put(wKey, extendedWIndex);
        }

        int len = length(extendedVIndex, extendedWIndex, true);
        if (len == -1)
            return len;
        else
            return len - 2;

        // SAPData sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getLength();
        // }
        // else {
        // key = toKey(w, v);
        // sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getLength();
        // }
        // }
        // return helper(key, vKey, wKey, v, w, 0);
    }

    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        String vKey = toKey(v), wKey = toKey(w);

        if (vKey.equals(wKey)) {
            if (v.iterator().hasNext())
                return v.iterator().next();
            else if (w.iterator().hasNext())
                return w.iterator().next();
            else
                return -1;
        }

        // String key = vKey + "+" + wKey;
        Integer extendedVIndex = extendedMap.get(vKey);
        if (extendedVIndex == null) {
            extendedVIndex = ++this.extendedIndex;
            for (int vv : v) {
                // if (vv < 0 || vv >= this.G.V())
                // throw new IndexOutOfBoundsException();
                if (extendedVIndex >= G.V()) { // graph doubling
                    G = new MyDigraph(G, G.V() * 2);
                }
                G.addEdge(extendedVIndex, vv);
            }
        }

        Integer extendedWIndex = extendedMap.get(wKey);
        if (extendedWIndex == null) {
            extendedWIndex = ++this.extendedIndex;
            for (int ww : w) {
                // if (ww < 0 || ww >= this.G.V())
                // throw new IndexOutOfBoundsException();
                if (extendedWIndex >= G.V()) { // graph doubling
                    G = new MyDigraph(G, G.V() * 2);
                }
                G.addEdge(extendedWIndex, ww);
            }
        }

        return ancestor(extendedVIndex, extendedWIndex, true);

        // SAPData sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getAncestor();
        // }
        // else {
        // key = toKey(w, v);
        // sapData = queryCache.get(key);
        // if (sapData != null) {
        // return sapData.getAncestor();
        // }
        // }
        // return helper(key, vKey, wKey, v, w, 1);
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no
    // such path
    // private int helper(String key, String vKey, String wKey, Iterable<Integer> v, Iterable<Integer> w, int caller) {
    // SAPData sapData = null;
    //
    // MyBSF vBsf = new MyBSF(vKey, this.G, v);
    // MyBSF wBsf = new MyBSF(wKey, this.G, w);
    //
    // if (wBsf.isRecorded() && vBsf.isRecorded()) {
    // MyBSF temp = null;
    //
    // // System.out.println(vBsf.getDistTo().size() + " vs " + wBsf.getDistTo().size());
    // if (vBsf.getDistTo().size() > wBsf.getDistTo().size()) {
    // temp = vBsf;
    // vBsf = wBsf;
    // wBsf = temp;
    // }
    // }
    //
    // for (int vv : vBsf.getDistTo()) {
    // if (wBsf.visited(vv)) {
    // if (sapData == null || sapData.getLength() > (wBsf.distTo(vv) + vBsf.distTo(vv))) {
    // sapData = new SAPData(wBsf.distTo(vv) + vBsf.distTo(vv), vv);
    // // break;
    // }
    // }
    // }
    //
    // if (sapData == null) {
    // sapData = lockstep(vBsf, wBsf);
    // } else if (sapData.getLength() > 0 && !(vBsf.isTerminated() && wBsf.isTerminated())) {
    // SAPData deep = lockstep(vBsf, wBsf);
    // if (deep != null && sapData.getLength() > deep.getLength())
    // sapData = deep;
    // }
    //
    // if (!vBsf.isTerminated())
    // vBsf.terminate();
    // if (!wBsf.isTerminated())
    // wBsf.terminate();
    //
    // if (sapData != null) {
    // queryCache.put(key, sapData);
    //
    // if (caller == 1)
    // return sapData.getAncestor();
    // else
    // return sapData.getLength();
    // } else {
    // queryCache.put(key, new SAPData(NOT_FOUND, NOT_FOUND));
    // return NOT_FOUND;
    // }
    // }

    private int helper(String key, int v, int w, int caller) {
        SAPData sapData = null;

        MyBSF vBsf = new MyBSF(toKey(v), this.G, v);
        MyBSF wBsf = new MyBSF(toKey(w), this.G, w);

        for (int vv : vBsf.getDistTo()) {
            if (wBsf.visited(vv)) {
                if (sapData == null || sapData.getLength() > (wBsf.distTo(vv) + vBsf.distTo(vv))) {
                    sapData = new SAPData(wBsf.distTo(vv) + vBsf.distTo(vv), vv);
                    // break;
                }
            }
        }

        if (sapData == null) {
            sapData = lockstep(vBsf, wBsf);
        } else if (sapData.getLength() > 0 && !(vBsf.isTerminated() && wBsf.isTerminated())) {
            SAPData deep = lockstep(vBsf, wBsf);
            if (deep != null && sapData.getLength() > deep.getLength())
                sapData = deep;
        }

        if (!vBsf.isTerminated())
            vBsf.terminate();
        if (!wBsf.isTerminated())
            wBsf.terminate();

        if (sapData != null) {
            queryCache.put(key, sapData);
            if (caller == 1)
                return sapData.getAncestor();
            else
                return sapData.getLength();
        } else {
            queryCache.put(key, new SAPData(NOT_FOUND, NOT_FOUND));
            return NOT_FOUND;
        }
    }

    private SAPData lockstep(MyBSF vBsf, MyBSF wBsf) {
        SAPData sapData = null;

        int[] vAdj = null;
        int[] wAdj = null;
        // walk both sides
        while (vBsf.hasNext() || wBsf.hasNext()) {
            vAdj = vBsf.next();
            wAdj = wBsf.next();

            if (vAdj != null) {
                for (int i = 0; i < vAdj.length; ++i) {
                    int b = vAdj[i];
                    if (wBsf.visited(b)) {
                        int length = vBsf.distTo(b) + wBsf.distTo(b);
                        if (sapData == null || sapData.getLength() > length) {
                            sapData = new SAPData(length, b);
                        }
                        if (sapData == null || vBsf.distTo(b) >= sapData.getLength()) {
                            vBsf.terminate();
                            break;
                        }
                    }
                }
            }

            if (wAdj != null) {
                for (int i = 0; i < wAdj.length; ++i) {
                    int b = wAdj[i];
                    if (vBsf.visited(b)) {
                        int length = vBsf.distTo(b) + wBsf.distTo(b);
                        if (sapData == null || sapData.getLength() > length) {
                            sapData = new SAPData(length, b);
                        }
                        if (sapData == null || wBsf.distTo(b) >= sapData.getLength()) {
                            wBsf.terminate();
                            break;
                        }
                    }
                }
            }
            // else {
            // System.out.println("w ended");
            // }
        }
        return sapData;
    }

    private String toKey(Iterable<Integer> v) {
        StringJoiner sa = new StringJoiner(",");
        v.forEach((vv) -> {
            sa.add(String.valueOf(vv));
        });

        return sa.toString();
    }

    private String toKey(int v, int w) {
        return v + ":" + w;
    }

    private String toKey(int v) {
        return String.valueOf(v);
    }

    // do unit testing of this class
    public static void main(String[] args) {
    }
}