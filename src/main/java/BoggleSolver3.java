import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;

import edu.princeton.cs.algs4.In;

public class BoggleSolver3 {
    private MyTrieSET dictionary;
    private static final boolean DEBUG = false;
    private int tag;

    // Initializes the data structure using the given array of strings as the dictionary.
    // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver3(String[] dictionary) {
        this.dictionary = new MyTrieSET();
        for (String word : dictionary) {
            if (word.length() >= 3) {
                this.dictionary.add(word);
            }
        }
    }

    private static class MyTrieSET {
        private static final int R = 26; // A-Z

        private Node root; // root of trie
        private int N; // number of keys in trie

        // R-way trie node
        public static class Node {
            private final Node[] next = new Node[R];
            private String string;
            private int tag;
            public boolean hasChild;
        }

        /**
         * Initializes an empty set of strings.
         */
        public MyTrieSET() {
        }

        private char offset(char c) {
            return (char) (c - 'A');
        }

        /**
         * Does the set contain the given key?
         * 
         * @param key
         *            the key
         * @return <tt>true</tt> if the set contains <tt>key</tt> and <tt>false</tt> otherwise
         * @throws NullPointerException
         *             if <tt>key</tt> is <tt>null</tt>
         */
        public int contains(Node node) {
            if (node == null)
                return 0;
            if (node.string != null)
                return 2;
            else
                return 1;
        }

        public Node get(Node x, int key) {
            return x.next[key];
        }

        public Node get(Node x, String key) {
            Node n = x;
            for (int i = 0; i < key.length(); ++i) {
                n = get(n, offset(key.charAt(i)));
                if (n == null)
                    break;
            }
            return n;
        }

        /**
         * Adds the key to the set if it is not already present.
         * 
         * @param key
         *            the key to add
         * @throws NullPointerException
         *             if <tt>key</tt> is <tt>null</tt>
         */
        public void add(String key) {
            root = add(root, key, 0);
        }

        private Node add(Node x, String key, int d) {
            if (x == null)
                x = new Node();
            if (d == key.length()) {
                if (x.string == null) {
                    N++;
                    x.string = key;
                }
            } else {
                char c = offset(key.charAt(d));
                x.next[c] = add(x.next[c], key, d + 1);
                x.hasChild = true;
            }
            return x;
        }

        /**
         * Returns the number of strings in the set.
         * 
         * @return the number of strings in the set
         */
        public int size() {
            return N;
        }

        /**
         * Is the set empty?
         * 
         * @return <tt>true</tt> if the set is empty, and <tt>false</tt> otherwise
         */
        public boolean isEmpty() {
            return size() == 0;
        }

        public Node getRoot() {
            return root;
        }
    }

    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {
        if (DEBUG)
            System.out.println("solving board: \n" + board);

        if (dictionary.isEmpty()) {
            return Arrays.asList();
        }

        tag += 1;
        return dfs(board);
    }

    private int[] myBoard;
    private int myBoardRowLimit;
    private int myBoardColLimit;
    private int[][] adjacents;
    private Iterable<String> dfs(BoggleBoard board) {
        final List<String> validWords = new LinkedList<String>();
        // pre-compute board
        int boardSize = board.rows() * board.cols();
        myBoard = new int[boardSize];
        myBoardRowLimit = board.rows() - 1;
        myBoardColLimit = board.cols() - 1;
        adjacents = new int[boardSize][8];
        int pos = 0;
        for (int row = 0; row < board.rows(); ++row) {
            for (int col = 0; col < board.cols(); ++col) {
                int i = 0;
                myBoard[pos] = board.getLetter(row, col) - 'A';
                
                if (row > 0) {
                    // N
                    adjacents[pos][i++] = pos - board.cols();
                    if (col < myBoardColLimit) {
                        // NE
                        adjacents[pos][i++] = pos - board.cols() + 1;
                    }
                }

                if (col < myBoardColLimit) {
                    // E
                    adjacents[pos][i++] = pos + 1;
                    if (row < myBoardRowLimit) {
                        // SE
                        adjacents[pos][i++] = pos + board.cols() + 1;
                    }
                }

                if (row < myBoardRowLimit) {
                    // S
                    adjacents[pos][i++] = pos + board.cols();
                    if (col > 0) {
                        // SW
                        adjacents[pos][i++] = pos + board.cols() - 1;
                    }
                }

                if (col > 0) {
                    // W
                    adjacents[pos][i++] = pos - 1;
                    if (row > 0) {
                        // NW
                        adjacents[pos][i++] = pos - board.cols() - 1;
                    }
                }
                
                for (int j=i; j<8; ++j) {
                    adjacents[pos][j] = -1;
                }

                ++pos;
            }
        }
        
        for (int i = 0; i < myBoard.length; ++i) {
            myBoard[i] = -1;
            
            MyTrieSET.Node myDictNode = dictionary.getRoot();
            final int c = myBoard[i];
            
            if (c == 16) { // 'Q'
                myDictNode = dictionary.get(myDictNode, c); 
                int contains = dictionary.contains(myDictNode);
                if (contains == 0) {
                    if (DEBUG) System.out.println(myDictNode.string + " not found in dictionary");
                    continue;
                }
                myDictNode = dictionary.get(myDictNode, 20);
            }else {
                myDictNode = dictionary.get(myDictNode, c);
            }
            
            // stop going down further
            int contains = dictionary.contains(myDictNode);
            if (contains == 0) {
                if (DEBUG) System.out.println(myDictNode.string + " not found in dictionary");
                continue;
            } else
            if (contains == 2 && myDictNode.tag != this.tag) {
                validWords.add(myDictNode.string);
                myDictNode.tag = this.tag;
                // keep going
                if (!myDictNode.hasChild) continue;
            }
            
            helper(i, myDictNode, validWords);
            
            myBoard[i] = c;
        }

        return validWords;
    }

    private void helper(final int i, final MyTrieSET.Node dictNode,final List<String> validWords) {

        int[] adj = adjacents[i];
        int nextAdj;
        for(int j=0; j<adj.length; ++j) {
            nextAdj = adj[j];
            if(nextAdj == -1) break; 
            if(myBoard[nextAdj]==-1) continue;


            myBoard[i] = -1;

            MyTrieSET.Node myDictNode = dictNode;
            final int c = myBoard[nextAdj];
            
            if (c == 16) { // 'Q'
                myDictNode = dictionary.get(myDictNode, c); 
                int contains = dictionary.contains(myDictNode);
                if (contains == 0) {
                    if (DEBUG) System.out.println(myDictNode.string + " not found in dictionary");
                    continue;
                }
                myDictNode = dictionary.get(myDictNode, 20);
            }else {
                myDictNode = dictionary.get(myDictNode, c);
            }
            
            // stop going down further
            int contains = dictionary.contains(myDictNode);
            if (contains == 0) {
                if (DEBUG) System.out.println(myDictNode.string + " not found in dictionary");
                continue;
            } else
            if (contains == 2 && myDictNode.tag != this.tag) {
                validWords.add(myDictNode.string);
                myDictNode.tag = this.tag;
                // keep going
                if (!myDictNode.hasChild) continue;
            }
            
            helper(nextAdj, myDictNode, validWords);

            myBoard[i] = c;
        }
        return;
    }

    // Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        if (dictionary.contains(dictionary.get(dictionary.getRoot(), word)) != 2)
            return 0;

        int n = word.length();

        if (n <= 2)
            return 0;
        else if (n <= 4)
            return 1;
        else if (n == 5)
            return 2;
        else if (n == 6)
            return 3;
        else if (n == 7)
            return 5;
        else
            return 11;
    }

    public static void main(String[] args) {
//        args = new String[] { "./boggle/dictionary-yawl.txt", "./boggle/board-points777.txt" };
//        // args = new String[] { "./boggle/dictionary-yawl.txt", "./boggle/board-dichlorodiphenyltrichloroethanes.txt" };
//
//        In in = new In(args[0]);
//        String[] dictionary = in.readAllStrings();
//        BoggleSolver solver = new BoggleSolver(dictionary);
//        BoggleBoard board = new BoggleBoard(args[1]);
//        int score = 0;
//        for (String word : solver.getAllValidWords(board)) {
//            StdOut.println(word);
//            score += solver.scoreOf(word);
//        }
//        StdOut.println("Score = " + score);

        In in = new In("./boggle/dictionary-yawl.txt");
        String[] dictionary = in.readAllStrings();
        BoggleSolver3 solver = new BoggleSolver3(dictionary);

        Runnable task = new Runnable() {
            @Override
            public void run() {
                int score = 0;
                BoggleBoard board = null;
                LongAdder adder = new LongAdder();
                long before = System.currentTimeMillis();
                while (score < 4540) {
                    score = 0;
                    board = new BoggleBoard(4, 4);
                    adder.increment();
                    for (String word : solver.getAllValidWords(board)) {
                        score += solver.scoreOf(word);
                    }

                    if (score > 2000) {
                        System.out.println(score);
                        System.out.println(board);
                    }

                    if (System.currentTimeMillis() - before > 30000L) {
                        System.out.println(adder.longValue() / 30);
                        adder.reset();
                        before = System.currentTimeMillis();
                    }
                }
            }
        };

        for (int i = 0; i < 1; ++i) {
            new Thread(task).start();
        }
    }
}
